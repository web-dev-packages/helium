from database import Base
from sqlalchemy import Column, Integer, String

class User(Base):
	__tablename__ = 'users'
	id = Column(Integer, primary_key=True)
	firstname = Column(String(255), nullable=False)
	lastname = Column(String(255), nullable=False)
	email = Column(String(255), unique=True, nullable=False)
	password = Column(String(255), nullable=False)

	def __init__(self, **kwargs):
		self.firstname = kwargs.get('firstname')
		self.lastname = kwargs.get('lastname')
		self.email = kwargs.get('email')
		self.password = kwargs.get('password')

	def __repr__(self):
		return '<User {}>'.format(self.firstname)