import os
import sys
import logging
from flask import Flask
from flask_cors import CORS
from flask_restful import Api
from dotenv import load_dotenv
from flask_sqlalchemy import SQLAlchemy
from database import db_session, init_db
from flask_http_middleware import MiddlewareManager
from utils.common import register_routes, register_middlewares, register_models

load_dotenv()

logging.basicConfig(filename='storage/logs/debug.log', filemode='w', format='%(name)s - %(message)s', level=logging.DEBUG)
logging.basicConfig(filename='storage/logs/error.log', filemode='w', format='%(name)s - %(message)s', level=logging.ERROR)
logging.basicConfig(filename='storage/logs/warning.log', filemode='w', format='%(name)s - %(message)s', level=logging.WARNING)
logging.basicConfig(filename='storage/logs/critical.log', filemode='w', format='%(name)s - %(message)s', level=logging.CRITICAL)

app = Flask(__name__)
api = Api(app)
app.wsgi_app = MiddlewareManager(app)
CORS(app)

# -- middlewares --
register_middlewares(app)

# -- routes --
register_routes(app)

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

def main():
	# -- models --
	register_models()

	with app.app_context():
		init_db()

	app.run(
		host=os.getenv('APP_HOST'),
		port=os.getenv('APP_PORT'),
		debug=True
	)


if __name__ == '__main__':
	main()