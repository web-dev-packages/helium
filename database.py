import os
from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

load_dotenv()

db_config = 'mysql://' + str(os.getenv('MYSQL_USERNAME')) + ':' + str(os.getenv('MYSQL_PASSWORD')) + '@' + str(os.getenv('MYSQL_HOST')) + '/' + str(os.getenv('MYSQL_DATABASE'))
engine = create_engine(db_config)
db_session = scoped_session(
	sessionmaker(autocommit=False, autoflush=False, bind=engine)
)
Base = declarative_base()
Base.query = db_session.query_property()

def init_db():
	Base.metadata.create_all(bind=engine)